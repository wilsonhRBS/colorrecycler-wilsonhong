package com.example.randomcolorrecycler.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.randomcolorrecycler.model.ColorRepo
import com.example.randomcolorrecycler.view.ColorStatesResource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ColorViewModel: ViewModel() {
    private val repo by lazy {ColorRepo}

    private var _colorList: MutableLiveData<ColorStatesResource> = MutableLiveData(ColorStatesResource.Loading)
    val colorList: LiveData<ColorStatesResource> get() = _colorList

    fun fetchColorList(len: Int) = viewModelScope.launch(Dispatchers.Main) {
        _colorList.value = ColorStatesResource.Loading
        _colorList.value = repo.fetchColorList(len)
    }
}