package com.example.randomcolorrecycler.view

import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.randomcolorrecycler.databinding.ColorListItemBinding
import com.example.randomcolorrecycler.toHexString

class ColorListAdapter: RecyclerView.Adapter<ColorListAdapter.ColorListViewHolder>() {
    private lateinit var colorList: List<Int>

    class ColorListViewHolder(
        private val binding: ColorListItemBinding
    ): RecyclerView.ViewHolder(binding.root) {
        fun apply(color: Int) {
            binding.colorCard.setCardBackgroundColor(color)
            binding.colorTv.text = color.toHexString()
            //Change the border color
            (binding.colorTv.background as GradientDrawable).setStroke(15, color)
            binding.root.setOnClickListener {
                val action = ColorFragmentHomeDirections.actionColorFragmentHomeToColorFragmentDisplay(color)
                binding.root.findNavController().navigate(action)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorListViewHolder {
        val binding = ColorListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ColorListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ColorListViewHolder, position: Int) {
        val color = colorList[position]
        holder.apply(color)
    }

    override fun getItemCount(): Int {
        return colorList.size
    }

    fun giveData(data: List<Int>) {
        colorList = data
    }
}