package com.example.randomcolorrecycler.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.randomcolorrecycler.databinding.ColorFragmentDisplayBinding
import com.example.randomcolorrecycler.getComplementaryColor
import com.example.randomcolorrecycler.toHexString

class ColorFragmentDisplay: Fragment() {
    private var _binding: ColorFragmentDisplayBinding? = null
    private val binding: ColorFragmentDisplayBinding get() = _binding!!

    private val args by navArgs<ColorFragmentDisplayArgs>()

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ColorFragmentDisplayBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() = with(binding) {
        cardDisplay.setCardBackgroundColor(args.colorID)
        cardTv.setTextColor(args.colorID.getComplementaryColor())
        cardTv.text = args.colorID.toHexString()

        btnHome.setOnClickListener {
            findNavController().navigateUp()
        }
    }

}