package com.example.randomcolorrecycler.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.randomcolorrecycler.R
import com.example.randomcolorrecycler.databinding.OpenGraphFragmentBinding
import com.example.randomcolorrecycler.model.opengraph.OpenGraphViewModel
import com.example.randomcolorrecycler.model.opengraph.OpenGraphViewModelFactory
import com.example.randomcolorrecycler.url_global
import com.google.android.material.snackbar.Snackbar

class OpenGraphFragment: Fragment() {
    private var _binding: OpenGraphFragmentBinding? = null
    private val binding: OpenGraphFragmentBinding get() = _binding!!

    private lateinit var ogViewModel: OpenGraphViewModel

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        ogViewModel = ViewModelProvider(this, OpenGraphViewModelFactory(url_global))[OpenGraphViewModel::class.java]

        val bind = OpenGraphFragmentBinding.inflate(inflater, container, false).also {
            _binding = it
        }
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
        //init listener
            btn2.setOnClickListener {
                findNavController().navigate(OpenGraphFragmentDirections.actionOpenGraphFragmentToColorFragmentHome())
            }
            btn1.setOnClickListener {
                //fetch OG
                ogViewModel.parseOG()
            }
        //init observer
            ogViewModel.og_result.observe(viewLifecycleOwner) {
                //Snackbar.make(binding.root, "OBSERVED", Snackbar.LENGTH_SHORT).show()
                ogTitle.text = getString(R.string.og_title, it.title)
                ogDescription.text = getString(R.string.og_description, it.description)
                ogUrl.text = getString(R.string.og_url, it.url)
                ogImage.text = getString(R.string.og_image, it.image)
                ogSiteName.text = getString(R.string.og_siteName, it.siteName)
                ogType.text = getString(R.string.og_type, it.type)
            }
        }
    }
}