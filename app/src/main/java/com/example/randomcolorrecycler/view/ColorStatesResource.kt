package com.example.randomcolorrecycler.view

sealed class ColorStatesResource {
    data class Success(val data: List<Int>): ColorStatesResource()
    object Loading: ColorStatesResource()
    data class Error(val message: String): ColorStatesResource()
}