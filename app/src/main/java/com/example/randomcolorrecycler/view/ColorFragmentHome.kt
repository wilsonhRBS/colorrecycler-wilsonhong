package com.example.randomcolorrecycler.view

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.randomcolorrecycler.R
import com.example.randomcolorrecycler.databinding.ColorFragmentHomeBinding
import com.example.randomcolorrecycler.delay_time
import com.example.randomcolorrecycler.viewmodel.ColorViewModel
import com.example.randomcolorrecycler.viewmodel.ColorViewModelFactory
import com.google.android.material.snackbar.Snackbar

class ColorFragmentHome: Fragment() {
    private var _binding: ColorFragmentHomeBinding? = null
    private val binding: ColorFragmentHomeBinding get() = _binding!!

    private lateinit var colorViewModel: ColorViewModel

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        colorViewModel = ViewModelProvider(this, ColorViewModelFactory())[ColorViewModel::class.java]
        val bind = ColorFragmentHomeBinding.inflate(inflater, container, false)
        _binding = bind
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
        //initial fetch
        colorViewModel.fetchColorList(binding.inputLen.text.toString().toInt())
    }

    private fun initListeners() = with(colorViewModel) {
        colorList.observe(viewLifecycleOwner) { resource ->
            when(resource) {
                ColorStatesResource.Loading -> {
                    //Snackbar.make(requireContext(), binding.recycler, "Loading", Snackbar.LENGTH_SHORT).show()
                    binding.progress.isVisible = true
                    binding.menuBtn1.isEnabled = false
                }
                is ColorStatesResource.Success -> {
                    binding.progress.isVisible = false
                    binding.menuBtn1.isEnabled = true
                    binding.recycler.layoutManager = LinearLayoutManager(requireContext())
                    binding.recycler.adapter = ColorListAdapter().apply {
                        giveData(resource.data)
                    }
                }
                is ColorStatesResource.Error -> {
                    Snackbar.make(requireContext(), binding.recycler, "Error: " + resource.message, Snackbar.LENGTH_SHORT).show()
                }
            }
        }

        binding.menuBtn1.setOnClickListener {
            val len = binding.inputLen.text.toString().toInt()
            displayConfirmDialog(len)
        }
        binding.menuBtn2.setOnClickListener {
            Snackbar.make(requireContext(), binding.recycler, "Cancel Button", Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun displayConfirmDialog(len: Int) {
        val adb = AlertDialog.Builder(requireContext())
        adb.setTitle(R.string.app_name)
        adb.setMessage(getString(R.string.dialog_confirm_color, ((len * delay_time).toFloat() / 1000f).toString()))
        adb.setPositiveButton(R.string.confirm) { _, _ ->
            colorViewModel.fetchColorList(len)
        }
        adb.setNegativeButton(android.R.string.cancel) { _, _ ->
            Snackbar.make(binding.root, getString(android.R.string.cancel), Snackbar.LENGTH_SHORT).show()
        }
        adb.show()
    }
}