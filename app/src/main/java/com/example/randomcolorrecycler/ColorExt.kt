package com.example.randomcolorrecycler

import android.graphics.Color

const val delay_time = 50L
const val url_global = "https://gitlab.com/wilsonhRBS/colorrecycler-wilsonhong"

fun Int.toHexString(): String {
    Color.red(this)
    return String.format("#%02X%02X%02X", Color.red(this), Color.green(this), Color.blue(this))
}

fun Int.getComplementaryColor(): Int{
    val r = Color.red(this)
    val g = Color.green(this)
    val b = Color.blue(this)
    val big = largest(arrayOf(r,g,b))
    val small = smallest(arrayOf(r,g,b))
    val rr = big + small - r
    val gg = big + small - g
    val bb = big + small - b
    return Color.rgb(rr,gg,bb)
}

fun largest (arr: Array<Int>): Int {
    var largest = arr[0]
    for (num in arr) {
        if (largest < num)
            largest = num
    }
    return largest
}

fun smallest (arr: Array<Int>): Int {
    var smallest = arr[0]
    for (num in arr) {
        if (smallest > num)
            smallest = num
    }
    return smallest
}