package com.example.randomcolorrecycler.model.opengraph

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jsoup.Jsoup

class OpenGraphViewModel(private var url: String): ViewModel() {
    private val AGENT = "Mozilla"
    private val REFERRER = "http://www.google.com"
    private val TIMEOUT = 10000
    private val DOC_SELECT_QUERY = "meta[property^=og:]"
    private val OPEN_GRAPH_KEY = "content"
    private val PROPERTY = "property"
    private val OG_IMAGE = "og:image"
    private val OG_DESCRIPTION = "og:description"
    private val OG_URL = "og:url"
    private val OG_TITLE = "og:title"
    private val OG_SITE_NAME = "og:site_name"
    private val OG_TYPE = "og:type"

    private var _og_result = MutableLiveData(OpenGraphResult())
    val og_result: LiveData<OpenGraphResult> get() = _og_result

    fun parseOG() = viewModelScope.launch(Dispatchers.Main) {
        if (!url.contains("http")) {
            url = "http://$url"
        }

        val openGraphResult = OpenGraphResult()

        Log.e("OGVM", "Attempt connection")
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = Jsoup.connect(url)
                    .ignoreContentType(true)
                    .userAgent(AGENT)
                    .referrer(REFERRER)
                    .timeout(TIMEOUT)
                    .followRedirects(true)
                    .execute()

                Log.e("OGVM", "Successful connection")
                val doc = response.parse()

                val ogTags = doc.select(DOC_SELECT_QUERY)
                when {
                    ogTags.size > 0 ->
                        ogTags.forEachIndexed { index, _ ->
                            val tag = ogTags[index]
                            val text = tag.attr(PROPERTY)

                            when (text) {
                                OG_IMAGE -> {
                                    openGraphResult.image = (tag.attr(OPEN_GRAPH_KEY))
                                }
                                OG_DESCRIPTION -> {
                                    openGraphResult.description = (tag.attr(OPEN_GRAPH_KEY))
                                }
                                OG_URL -> {
                                    openGraphResult.url = (tag.attr(OPEN_GRAPH_KEY))
                                }
                                OG_TITLE -> {
                                    openGraphResult.title = (tag.attr(OPEN_GRAPH_KEY))
                                }
                                OG_SITE_NAME -> {
                                    openGraphResult.siteName = (tag.attr(OPEN_GRAPH_KEY))
                                }
                                OG_TYPE -> {
                                    openGraphResult.type = (tag.attr(OPEN_GRAPH_KEY))
                                }
                            }
                        }
                }
                Log.e("OGVM", "1: " + openGraphResult.toString())
                _og_result.postValue(openGraphResult)
                Log.e("OGVM", "2: " + _og_result.value.toString())
            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("OGVM", e.toString())
                viewModelScope.launch(Dispatchers.Main) {
                    //listener.onError(e.localizedMessage)
                }
            }
        }
    }
}