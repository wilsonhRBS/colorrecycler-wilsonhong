package com.example.randomcolorrecycler.model

import android.graphics.Color
import com.example.randomcolorrecycler.delay_time
import com.example.randomcolorrecycler.view.ColorStatesResource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import kotlin.random.Random

object ColorRepo {
    private val colorApi: ColorApi = object: ColorApi {
        override fun getColorList(len: Int): List<Int> {
            return IntArray(len) { getRandomColor() }.asList()
        }

        private fun  getRandomColor(): Int {
            return Color.rgb(Random.nextInt(255), Random.nextInt(255), Random.nextInt(255))
        }
    }

    suspend fun fetchColorList(len: Int) = withContext(Dispatchers.IO) {
        delay(len * delay_time)
        return@withContext ColorStatesResource.Success(colorApi.getColorList(len))
    }
}