package com.example.randomcolorrecycler.model

interface ColorApi {
    fun getColorList(len: Int): List<Int>
}