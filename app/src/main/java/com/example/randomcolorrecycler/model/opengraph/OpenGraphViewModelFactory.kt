package com.example.randomcolorrecycler.model.opengraph

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class OpenGraphViewModelFactory(private var url: String): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(OpenGraphViewModel::class.java)) {
            return OpenGraphViewModel(url) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}